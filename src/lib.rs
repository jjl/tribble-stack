#[cfg(feature="std")]
use std as alloc;
#[cfg(not(feature="std"))]
extern crate alloc;

use core::cell::{Cell, UnsafeCell};
use core::fmt::{self, Debug};
use core::mem::{forget, MaybeUninit};
use core::ptr::{drop_in_place, null_mut, NonNull};
use core::sync::atomic::{AtomicPtr, Ordering::*};
use alloc::alloc::{alloc, dealloc, Layout};
use crossbeam_utils::CachePadded;

pub struct Oom;

/// A small and fast Treiber stack.
///
/// Important: Not ABA safe! When you take a message off the stack, readers may still be trying to
/// access it. It's thus very important to delay freeing it.
pub struct Stack<T> {
  head:  CachePadded<AtomicPtr<Slot<T>>>,
}

impl<T> Default for Stack<T> {
  fn default() -> Self {
    Stack { head: CachePadded::new(AtomicPtr::new(null_mut())) }
  }
}

impl<T> Stack<T> {
  pub fn push(&self, message: Message<T>) {
    let mut head = self.head.load(Relaxed); // Relaxed: no read.
    loop {
      unsafe { &*message.0.as_ptr() }.prev.set(head); // exclusive owernership until we win.
      match self.head.compare_exchange_weak(head, message.0.as_ptr(), Release, Relaxed) {
        Ok(_)    => { return }     // release new node, done.
        Err(new) => { head = new } // try again with new head
      }
    }
  }
  /// ## Safety
  ///
  /// You must wait before freeing the capsule surrounding the message to give other threads time to
  /// stop accessing it.
  pub unsafe fn pop(&self) -> Option<Message<T>> {
    let mut head = self.head.load(Acquire); // acquire: for read
    loop {
      if head.is_null() { return None }
      let prev = unsafe { &*head }.prev.get();
      match self.head.compare_exchange_weak(head, prev, Relaxed, Acquire) {
        Ok(_)    => { return Some(Message(NonNull::new_unchecked(head))) } // relaxed: no writes to sync.
        Err(new) => { head = new }                      // acquire new head
      }
    }
  }
}

impl<T> Drop for Stack<T> {
  fn drop(&mut self) {
    // we've got a mutable reference here, so we must be uniquely owned. that means we can acquire
    // once and not worry about any changes underneath us.
    let mut head = self.head.load(Acquire);
    while !head.is_null() {
      let prev = unsafe { &*head }.prev.get();
      unsafe { Slot::drop_slot(NonNull::new_unchecked(prev)) };
      head = prev;
    }
  }
}

/// it is safe for the queue to be sent to another thread so long as any values contained are
unsafe impl<T: Send> Send for Stack<T> {}
/// since you can push and pull from a ref, T must be Send. It does not need to be sync because we
/// never have a ref.
unsafe impl<T: Send> Sync for Stack<T> {}

/// An empty holder of a single message.
#[repr(transparent)]
pub struct Capsule<T>(NonNull<Slot<T>>);

impl<T> Capsule<T> {
  #[inline(always)]
  pub fn new() -> Capsule<T> {
    Capsule(Slot::new(null_mut(), MaybeUninit::uninit()))
  }
  #[inline(always)]
  pub fn try_new() -> Result<Capsule<T>, Oom> {
    Slot::try_new(null_mut(), MaybeUninit::uninit()).map(Capsule)
  }
}

impl<T> Capsule<T> {
  #[inline(always)]
  pub fn pack(self, value: T) -> Message<T> { Message::pack(value, self) }
}

impl<T> Drop for Capsule<T> {
  #[inline(always)]
  fn drop(&mut self) { unsafe { Slot::drop_slot(self.0) } }
}

impl<T> Default for Capsule<T> {
  #[inline(always)]
  fn default() -> Self { Self::new() }
}

/// A holder of a single message, ready for transmission.
pub struct Message<T>(NonNull<Slot<T>>);
  
impl<T> Drop for Message<T> {
  #[inline(always)]
  fn drop(&mut self) {
    unsafe {
      drop_in_place((*self.0.as_ptr()).data.get().cast::<T>());
      Slot::drop_slot(self.0);
    }
  }
}

impl<T> Message<T> {
  #[inline(always)]
  pub fn new(value: T) -> Self {
    Message(Slot::new(null_mut(), MaybeUninit::new(value)))
  }
  #[inline(always)]
  pub fn try_new(value: T) -> Result<Message<T>, Oom> {
    Slot::try_new(null_mut(), MaybeUninit::new(value)).map(Message)
  }
  #[inline(always)]
  pub fn pack(value: T, capsule: Capsule<T>) -> Self {
    unsafe { (*capsule.0.as_ptr()).data.get().write(MaybeUninit::new(value)) };
    let ret = Message(capsule.0);
    forget(capsule);
    ret
  }
  #[inline(always)]
  pub fn unpack(self) -> (T, Capsule<T>) {
    let value = unsafe { (*self.0.as_ptr()).data.get().read().assume_init() };
    let capsule = Capsule(self.0);
    forget(self);
    (value, capsule)
  }
}

impl<T: Debug> Debug for Message<T> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
    let it = unsafe { &*self.0.as_ptr() };
    f.debug_struct("Message")
     .field("address",  &self.0)
     .field("previous", &it.prev.get())
     .field("data",     &it.data)
     .finish()
  }
}

struct Slot<T> {
  prev: Cell<*mut Slot<T>>,
  data: UnsafeCell<MaybeUninit<T>>,
}

impl<T> Slot<T> {
  #[inline(always)]
  fn new(prev: *mut Slot<T>, data: MaybeUninit<T>) -> NonNull<Self> {
    let slot = Slot { prev: Cell::new(prev), data: UnsafeCell::new(data) };
    unsafe { NonNull::new_unchecked(Box::leak(Box::new(slot))) }
  }
  #[inline(always)]
  fn try_new(prev: *mut Slot<T>, data: MaybeUninit<T>) -> Result<NonNull<Self>, Oom> {
    let layout = Layout::new::<Slot<T>>();
    let ptr = unsafe { alloc(layout) }.cast::<Slot<T>>();
    if !ptr.is_null() {
      unsafe { ptr.write(Slot { prev: Cell::new(prev), data: UnsafeCell::new(data) }) };
      Ok(unsafe { NonNull::new_unchecked(ptr.cast()) })
    } else {
      Err(Oom)
    }
  }
  #[inline(always)]
  unsafe fn drop_slot(ptr: NonNull<Slot<T>>) {
    dealloc(ptr.as_ptr().cast(), Layout::new::<Slot<T>>())
  }
}


